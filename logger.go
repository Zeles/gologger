package gologger

import (
	"log"
	"os"
)

type LogType int8

const (
	StdOut LogType = 0
	Usage LogType = 1
	Error LogType = 2
	Fatal LogType = 3
)

func (t LogType) ToString() string {
	typestring := []string{"Std", "Usage", "Error", "Fatal"}
	if t < 0 || t > 2 {
		return "Unknow"
	}
	return typestring[t]
}

func (t LogType) Logger(massege string) {
	log.Println(t.ToString(), "\t", massege)
	if t == Fatal {
		os.Exit(1)
	}
}
